<?php

namespace Solnet\Elemental\Fluent;

use \TractorCow\Fluent\Model\Locale;
use \SilverStripe\ORM\DataExtension;

/**
 * Adds behaviour to a page enabling smooth CMS usage with both the Fluent and Elemental modules
 * installed.
 */

class ElementalFluentPageExtension extends DataExtension
{
    /**
     * If we are creating a new localised version of the page, ensure the page's
     * existing elements are copied to the new page rather than directly linked.
     *
     * This avoids the situation where making changes on the newly created localised
     * copy also appears on the original page.
     *
     * @return void
     */
    public function onBeforeWrite()
    {
        if (!$this->owner->isDraftedInLocale()) {
            // Leverage the already-working duplicate() / cascade_duplicates for elementalareas.
            $elementalAreaNew = $this->owner->ElementalArea()->duplicate();
            $this->owner->ElementalAreaID = $elementalAreaNew->ID;
        }
    }

    /**
     * Override default Fluent fallback
     *
     * @param string $query  The query reference to alter
     * @param string $table  Table to select from
     * @param string $field  Field being selected
     * @param Locale $locale Locale being selected
     *
     * @return null
     */
    public function updateLocaliseSelect(&$query, $table, $field, Locale $locale)
    {
        // disallow elemental data inheritance in the case that published localised page instance already exists
        if ($field == 'ElementalAreaID' && $this->owner->isPublishedInLocale()) {
            $query = '"' . $table . '_Localised_' . $locale->getLocale() . '"."' . $field . '"';
        }
    }
}
